#!/bin/bash

#Add Whonix repo
echo "deb https://deb.whonix.org buster main contrib non-free" | sudo tee /etc/apt/sources.list.d/whonix.list

#Fetches official signing key
curl --tlsv1.2 --proto =https --max-time 180 --output ~/patrick.asc https://www.whonix.org/patrick.asc
sudo apt-key --keyring /etc/apt/trusted.gpg.d/whonix.gpg add ~/patrick.asc

#Installs the Tor Derivative browser
sudo apt-get update
sudo apt-get install --no-install-recommends secbrowser