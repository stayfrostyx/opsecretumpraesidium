# Linux Hardening Measures
## Hardening guide for debian-based linux distributions
### Initial Install script for hardening debian - DebSet - (Under Development)
### Debset can harden the linux kernel, ssh, file permissions, create a secure /tmp directory, disable uncommon network protocols, tune ufw(firewall), disable unneeded storage, etc
- chmod +x Debset
- sudo ./Debset


### Security on Boot - startupscript
- Run chmod +x ./startupscript
    - Optionally set up a cron job to automatically open on login

### Sources
- Boelen, M. (2019). Security Auditing and Compliance Solutions - CISOfy. [online] Cisofy.com. Available at: https://cisofy.com 
- CIS. (2019). CIS Benchmarks™. [online] Available at: https://www.cisecurity.org/cis-benchmarks/ 
- Debian. (2019). Index of /debian-cd/current/amd64/iso-cd. [online] Available at: https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/
- DISA (2019). Security Technical Implementation Guides (STIGs) – DoD Cyber Exchange. [online] Available at: https://public.cyber.mil/stigs/
- Kernsec.org. (2019). Kernel Self Protection Project/Recommended Settings - Linux Kernel Security Subsystem. Available at: https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings.
- Tails. (2020). Tails - Privacy for anyone anywhere. Retrieved from https://tails.boum.org/
- Whonix. (2019). Using apparmor-profile-everything on Debian Buster. [online] Available at: https://forums.whonix.org/t/using-apparmor-profile-everything-on-debian-buster/8650 
- Whonix. (2019). Whonix/whonix-repository. [online] Available at: https://github.com/Whonix/whonix-repository 
- Whonix. (2019, December 12). Workstation Security Hardening. Retrieved from https://www.whonix.org/wiki/Whonix-Workstation_Security_Hardening#Restrict_Hardware_Information_to_Root. 